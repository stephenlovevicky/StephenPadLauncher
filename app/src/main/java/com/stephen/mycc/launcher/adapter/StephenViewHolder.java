package com.stephen.mycc.launcher.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class StephenViewHolder {
    private final SparseArray<View> mViews;
    private int mPosition;
    private View mConvertView;

    private StephenViewHolder(Context context, ViewGroup parent, int layoutId, int position) {
        this.mPosition = position;
        this.mViews = new SparseArray<View>();
        mConvertView = LayoutInflater.from(context).inflate(layoutId, parent, false);
        mConvertView.setTag(this);//setTag
    }

    /**
     * 拿到一个ViewHolder对象
     */
    public static StephenViewHolder get(Context context, View convertView, ViewGroup parent, int layoutId, int position) {
        StephenViewHolder holder = null;
        if (convertView == null) {
            holder = new StephenViewHolder(context, parent, layoutId, position);
        } else {
            if(convertView.getTag() instanceof StephenViewHolder){
                holder = (StephenViewHolder) convertView.getTag();
                holder.mPosition = position;
            }//end of if
        }
        return holder;
    }

    public View getConvertView() {
        return mConvertView;
    }

    /**
     * 通过控件的Id获取对于的控件，如果没有则加入views
     */
    public <T extends View> T getView(int viewId) {
        View view = mViews.get(viewId);
        if (view == null) {
            view = mConvertView.findViewById(viewId);
            mViews.put(viewId, view);
        }//end of if
        return (T) view;
    }

    /**
     * 为TextView设置字符串
     */
    public StephenViewHolder setText(int viewId, String text) {
        TextView view = getView(viewId);
        view.setText(text);
        return this;
    }

    public StephenViewHolder setText(int viewId, int text) {
        TextView view = getView(viewId);
        view.setText(text);
        return this;
    }
    /**
     * 为ImageView设置图片
     */
    public StephenViewHolder setImageResource(int viewId, int drawableId) {
        ImageView view = getView(viewId);
        view.setImageResource(drawableId);
        return this;
    }

    /**
     * 为ImageView设置图片
     */
    public StephenViewHolder setImageBitmap(int viewId, Bitmap bm) {
        ImageView view = getView(viewId);
        view.setImageBitmap(bm);
        return this;
    }

    public int getPosition() {
        return mPosition;
    }
}
