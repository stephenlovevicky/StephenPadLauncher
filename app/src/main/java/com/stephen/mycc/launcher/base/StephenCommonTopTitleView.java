package com.stephen.mycc.launcher.base;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.stephen.mycc.launcher.ripple.StephenRippleFrameLayout;

public class StephenCommonTopTitleView {
    public static int TitleHeightForDp = 65,TitleTextSp = 14;
    private String TitleBottomLineColorHex = "#005ed6";
    private Activity activity;
    private View titleBottomLineV,leftView,centerView,rightView;
    private StephenRippleFrameLayout titleBgFy,titleLeftFy,titleRightFy,titleCenterFy;

    public StephenCommonTopTitleView(Activity activity) {
        this.activity = activity;
        initDefaultCommTitleView();
    }

    public StephenCommonTopTitleView(Activity activity, int titleHeightForDp) {
        this.activity = activity;
        this.TitleHeightForDp = titleHeightForDp;
        initDefaultCommTitleView();
    }

    private void initDefaultCommTitleView(){
        titleBgFy = new StephenRippleFrameLayout(activity);
        //titleBgFy.setBackgroundColor(Color.parseColor(StephenToolUtils.MasterColorHex));
        titleCenterFy = new StephenRippleFrameLayout(activity);
        titleCenterFy.setRippleSpeed(5);
        titleBgFy.addView(titleCenterFy,new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT,Gravity.CENTER));
        RelativeLayout titleCenterRy = new RelativeLayout(activity);
        titleCenterRy.setClipChildren(false);//ripple parent set
        titleCenterRy.setPadding(StephenToolUtils.dip2px(activity,TitleHeightForDp/9),0,StephenToolUtils.dip2px(activity,TitleHeightForDp/9),0);
        titleBgFy.addView(titleCenterRy,new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        titleLeftFy = new StephenRippleFrameLayout(activity);
        titleLeftFy.setRippleCanClip(false);
        titleLeftFy.setRippleSpeed(3);
        RelativeLayout.LayoutParams titleLeftFyLp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        titleLeftFyLp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        titleLeftFyLp.addRule(RelativeLayout.CENTER_VERTICAL);
        titleCenterRy.addView(titleLeftFy,titleLeftFyLp);
        titleRightFy = new StephenRippleFrameLayout(activity);
        titleRightFy.setRippleCanClip(false);
        titleRightFy.setRippleSpeed(3);
        RelativeLayout.LayoutParams titleRightFyLp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        titleRightFyLp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        titleRightFyLp.addRule(RelativeLayout.CENTER_VERTICAL);
        titleCenterRy.addView(titleRightFy,titleRightFyLp);
        titleBottomLineV = new View(activity);
        titleBottomLineV.setBackgroundColor(Color.parseColor(TitleBottomLineColorHex));
        titleBgFy.addView(titleBottomLineV,new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,StephenToolUtils.dip2px(activity,1),Gravity.BOTTOM));

        setTitleBottomLineVisibility(View.GONE);
        setTitleCenterVisibility(View.GONE);
        setTitleLeftVisibility(View.GONE);
        setTitleRightVisibility(View.GONE);
    }

    public View injectCommTitleViewToAllViewReturnView(int mainLayoutId){
        return injectCommTitleViewToAllViewReturnView(mainLayoutId,false,false);
    }
    public View injectCommTitleViewToAllViewReturnView(int mainLayoutId,boolean parentIsFrame,boolean isTopPadding) {//注入全部,返回view
        return injectCommTitleViewToAllViewReturnView(LayoutInflater.from(activity).inflate(mainLayoutId,null),parentIsFrame,isTopPadding);
    }
    public View injectCommTitleViewToAllViewReturnView(View mainView){
        return injectCommTitleViewToAllViewReturnView(mainView,false,false);
    }
    public View injectCommTitleViewToAllViewReturnView(View mainView,boolean parentIsFrame,boolean isTopPadding){//注入全部,返回view
        if(parentIsFrame){
            FrameLayout mainFy = new FrameLayout(activity);

            if(isTopPadding)mainView.setPadding(0,StephenToolUtils.dip2px(activity,TitleHeightForDp),0,0);
            if(null != mainView)mainFy.addView(mainView, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
            mainFy.addView(getTopTitleView(), FrameLayout.LayoutParams.MATCH_PARENT, StephenToolUtils.dip2px(activity,TitleHeightForDp));
            return mainFy;
        }else{
            LinearLayout mainLy = new LinearLayout(activity);
            mainLy.setOrientation(LinearLayout.VERTICAL);

            mainLy.addView(getTopTitleView(), LinearLayout.LayoutParams.MATCH_PARENT, StephenToolUtils.dip2px(activity,TitleHeightForDp));
            if(null != mainView)mainLy.addView(mainView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            return mainLy;
        }
    }
    //注入全部,直接用于activity
    public void injectCommTitleViewToAllViewWithActivity(int mainLayoutId){
        injectCommTitleViewToAllViewWithActivity(mainLayoutId,false,false);
    }
    public void injectCommTitleViewToAllViewWithActivity(int mainLayoutId,boolean parentIsFrame,boolean isTopPadding){
        View mainView = LayoutInflater.from(activity).inflate(mainLayoutId,null);
        if(null == mainView){
            activity.setContentView(mainLayoutId);
        }else{
            injectCommTitleViewToAllViewWithActivity(mainView,parentIsFrame,isTopPadding);
        }
    }
    //注入全部,直接用于activity
    public void injectCommTitleViewToAllViewWithActivity(View mainView){
        injectCommTitleViewToAllViewWithActivity(mainView,false,false);
    }
    public void injectCommTitleViewToAllViewWithActivity(View mainView,boolean parentIsFrame,boolean isTopPadding){
        if(parentIsFrame){
            FrameLayout mainFy = new FrameLayout(activity);

            if(isTopPadding)mainView.setPadding(0,StephenToolUtils.dip2px(activity,TitleHeightForDp),0,0);
            mainFy.addView(mainView, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
            mainFy.addView(getTopTitleView(), FrameLayout.LayoutParams.MATCH_PARENT, StephenToolUtils.dip2px(activity,TitleHeightForDp));
            activity.setContentView(mainFy);
        }else{
            LinearLayout mainLy = new LinearLayout(activity);
            mainLy.setOrientation(LinearLayout.VERTICAL);

            mainLy.addView(getTopTitleView(), LinearLayout.LayoutParams.MATCH_PARENT, StephenToolUtils.dip2px(activity,TitleHeightForDp));
            mainLy.addView(mainView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            activity.setContentView(mainLy);
        }
    }

    public FrameLayout getTopTitleView() {
        return titleBgFy;
    }

    public void setTitleBottomLineColor(int color){
        titleBottomLineV.setBackgroundColor(color);
    }

    public void setTitleBottomLineVisibility(int visibility){
        titleBottomLineV.setVisibility(visibility);
    }

    public int getTitleBottomLineVisibility(){
        return titleBottomLineV.getVisibility();
    }

    public void setTitleBgColor(int color){
        titleBgFy.setBackgroundColor(activity.getResources().getColor(color));
    }

    public void setTitleBgDrawable(Drawable bgDrawable){
        StephenToolUtils.setBackgroundAllVersion(titleBgFy,bgDrawable);
    }

    ///////////////////////////////////center

    public void setTitleCenterVisibility(int visibility){
        titleCenterFy.setVisibility(visibility);
    }

    public int getTitleCenterVisibility(){
        return titleCenterFy.getVisibility();
    }

    public void setTitleCenterText(String text){
        setTitleCenterText(text,null);
    }

    public void setTitleCenterText(String text,FrameLayout.LayoutParams lp){
        setTitleCenterText(null,text,-1,null,lp);
    }

    public void setTitleCenterText(String text,int textSizeSp,String colorHex){
        setTitleCenterText(null,text,textSizeSp,colorHex,null);
    }

    public void setTitleCenterText(String text,int textSizeSp,String colorHex,FrameLayout.LayoutParams lp){
        setTitleCenterText(null,text,textSizeSp,colorHex,lp);
    }

    public void setTitleCenterText(TextView textView,String text,int textSizeSp,String colorHex,FrameLayout.LayoutParams lp){
        if(null == textView){
            textView = new TextView(activity);
            textView.setGravity(Gravity.CENTER);
            textView.setSingleLine(true);
        }//end of if
        if(textSizeSp <= 0){
            textView.setTextSize(TitleHeightForDp/3);//sp
        }else{
            textView.setTextSize(textSizeSp);//sp
        }
        if(TextUtils.isEmpty(colorHex)){
            textView.setTextColor(Color.parseColor(StephenToolUtils.MasterFontColorHex));
        }else{
            textView.setTextColor(Color.parseColor(colorHex));
        }
        textView.setText(text);
        setTitleCenterView(textView,(null == lp) ? new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, Gravity.CENTER) : lp);
    }

    public void setTitleCenterIcon(int iconId){
        setTitleCenterIcon(iconId,new FrameLayout.LayoutParams(StephenToolUtils.dip2px(activity,TitleHeightForDp/3 * 2),StephenToolUtils.dip2px(activity,TitleHeightForDp/3 * 2),Gravity.CENTER));
    }

    public void setTitleCenterIcon(int iconId,FrameLayout.LayoutParams flParams){
        setTitleCenterIcon(null,iconId,flParams);
    }

    public void setTitleCenterIcon(ImageView imageView,int iconId,FrameLayout.LayoutParams flParams){
        if(null == imageView)imageView = new ImageView(activity);
        imageView.setImageDrawable(activity.getResources().getDrawable(iconId));
        setTitleCenterView(imageView,flParams);
    }

    public void setTitleCenterView(View curView,FrameLayout.LayoutParams flParams){
        if(getTitleCenterVisibility() != View.VISIBLE)setTitleCenterVisibility(View.VISIBLE);
        if(null != centerView){
            titleCenterFy.removeView(centerView);
            centerView = null;
        }//end of if
        if(null == centerView){
            centerView = curView;
            centerView.setId(StephenToolUtils.generateViewId());
            titleCenterFy.addView(centerView,flParams);
        }//end of if
    }

    public View getTitleCenterView() {
        return centerView;
    }

    public void setTitleCenterClickListener(View.OnClickListener onClickListener){
        titleCenterFy.setOnClickListener(onClickListener);
    }
    ///////////////////////////////////left

    public void setTitleLeftVisibility(int visibility){
        titleLeftFy.setVisibility(visibility);
    }

    public int getTitleLeftVisibility(){
        return titleLeftFy.getVisibility();
    }

    public void setTitleLeftText(String text){
        setTitleLeftText(text,null);
    }

    public void setTitleLeftText(String text,FrameLayout.LayoutParams lp){
        setTitleLeftText(null,text,-1,null,lp);
    }

    public void setTitleLeftText(String text,int textSizeSp,String colorHex){
        setTitleLeftText(null,text,textSizeSp,colorHex,null);
    }

    public void setTitleLeftText(String text,int textSizeSp,String colorHex,FrameLayout.LayoutParams lp){
        setTitleLeftText(null,text,textSizeSp,colorHex,lp);
    }

    public void setTitleLeftText(TextView textView,String text,int textSizeSp,String colorHex,FrameLayout.LayoutParams lp){
        if(null == textView){
            textView = new TextView(activity);
            textView.setGravity(Gravity.CENTER);
            textView.setSingleLine(true);
        }//end of if
        if(textSizeSp <= 0){
            textView.setTextSize(TitleHeightForDp/3);//sp
        }else{
            textView.setTextSize(textSizeSp);//sp
        }
        if(TextUtils.isEmpty(colorHex)){
            textView.setTextColor(Color.parseColor(StephenToolUtils.MasterFontColorHex));
        }else{
            textView.setTextColor(Color.parseColor(colorHex));
        }
        textView.setText(text);
        setTitleLeftView(textView,(null == lp) ? new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, Gravity.CENTER) : lp);
    }

    public void setTitleLeftIcon(int iconId){
        setTitleLeftIcon(iconId,new FrameLayout.LayoutParams(StephenToolUtils.dip2px(activity,TitleHeightForDp/9 * 4),StephenToolUtils.dip2px(activity,TitleHeightForDp/9 * 4),Gravity.CENTER));
    }

    public void setTitleLeftIcon(int iconId,FrameLayout.LayoutParams flParams){
        ImageView imageView = new ImageView(activity);
        imageView.setImageDrawable(activity.getResources().getDrawable(iconId));
        setTitleLeftView(imageView,flParams);
    }

    public void setTitleLeftView(View curView,FrameLayout.LayoutParams flParams){
        if(getTitleLeftVisibility() != View.VISIBLE)setTitleLeftVisibility(View.VISIBLE);
        if(null != leftView){
            titleLeftFy.removeView(leftView);
            leftView = null;
        }//end of if
        if(null == leftView){
            leftView = curView;
            leftView.setId(StephenToolUtils.generateViewId());
            titleLeftFy.addView(leftView,flParams);
        }//end of if
    }

    /*public void setTitleLeftIconAndText(int iconId,String text){
        setTitleLeftIconAndText(iconId,text,null);
    }
    public void setTitleLeftIconAndText(int iconId,String text,String colorHex){
        if(getTitleLeftVisibility() != View.VISIBLE)setTitleLeftVisibility(View.VISIBLE);
        if(null == leftTV){
            leftTV = new StephenHintPointTextView(activity);
            leftTV.setGravity(Gravity.CENTER);
            leftTV.setTextSize(TitleHeightForDp/3);//sp
            if(TextUtils.isEmpty(colorHex)){
                leftTV.setTextColor(Color.parseColor(StephenToolUtils.MasterFontColorHex));
            }else{
                leftTV.setTextColor(Color.parseColor(colorHex));
            }
            leftTV.setSingleLine(true);
            titleLeftFy.addView(leftTV,new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.MATCH_PARENT,Gravity.CENTER));
        }//end of if

        leftTV.setText(text);
        Drawable iconDrawable = activity.getResources().getDrawable(iconId);
        iconDrawable.setBounds(0,0, StephenToolUtils.dip2px(activity,TitleHeightForDp/9 * 5), StephenToolUtils.dip2px(activity,TitleHeightForDp/9 * 5));
        leftTV.setCompoundDrawablePadding(-StephenToolUtils.dip2px(activity,TitleHeightForDp/8));
        leftTV.setCompoundDrawables(iconDrawable, null, null, null);
    }*/

    public View getTitleLeftView() {
        return leftView;
    }

    public void setTitleLeftClickListener(View.OnClickListener onClickListener){
        titleLeftFy.setOnClickListener(onClickListener);
    }

    ///////////////////////////////////right

    public void setTitleRightVisibility(int visibility){
        titleRightFy.setVisibility(visibility);
    }

    public int getTitleRightVisibility(){
        return titleRightFy.getVisibility();
    }

    public void setTitleRightText(String text){
        setTitleRightText(text,null);
    }

    public void setTitleRightText(String text,FrameLayout.LayoutParams lp){
        setTitleRightText(null,text,-1,null,lp);
    }

    public void setTitleRightText(String text,int textSizeSp,String colorHex){
        setTitleRightText(null,text,textSizeSp,colorHex,null);
    }

    public void setTitleRightText(String text,int textSizeSp,String colorHex,FrameLayout.LayoutParams lp){
        setTitleRightText(null,text,textSizeSp,colorHex,lp);
    }

    public void setTitleRightText(TextView textView,String text,int textSizeSp,String colorHex,FrameLayout.LayoutParams lp){
        if(null == textView){
            textView = new TextView(activity);
            textView.setGravity(Gravity.CENTER);
            textView.setSingleLine(true);
        }//end of if
        if(textSizeSp <= 0){
            textView.setTextSize(TitleHeightForDp/3);//sp
        }else{
            textView.setTextSize(textSizeSp);//sp
        }
        if(TextUtils.isEmpty(colorHex)){
            textView.setTextColor(Color.parseColor(StephenToolUtils.MasterFontColorHex));
        }else{
            textView.setTextColor(Color.parseColor(colorHex));
        }
        textView.setText(text);
        setTitleRightView(textView,(null == lp) ? new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, Gravity.CENTER) : lp);
    }

    public void setTitleRightIcon(int iconId){
        setTitleRightIcon(iconId,new FrameLayout.LayoutParams(StephenToolUtils.dip2px(activity,TitleHeightForDp/9 * 4),StephenToolUtils.dip2px(activity,TitleHeightForDp/9 * 4),Gravity.CENTER));
    }

    public void setTitleRightIcon(int iconId,FrameLayout.LayoutParams flParams){
        setTitleRightIcon(null,iconId,flParams);
    }

    public void setTitleRightIcon(ImageView imageView,int iconId,FrameLayout.LayoutParams flParams){
        if(null == imageView)imageView = new ImageView(activity);
        imageView.setImageDrawable(activity.getResources().getDrawable(iconId));
        setTitleRightView(imageView,flParams);
    }

    public void setTitleRightIcon(Bitmap iconBmp){
        setTitleRightIcon(iconBmp,new FrameLayout.LayoutParams(StephenToolUtils.dip2px(activity,TitleHeightForDp/9 * 5),StephenToolUtils.dip2px(activity,TitleHeightForDp/9 * 5),Gravity.CENTER));
    }

    public void setTitleRightIcon(Bitmap iconBmp, FrameLayout.LayoutParams flParams){
        ImageView imageView = new ImageView(activity);
        imageView.setImageBitmap(iconBmp);
        setTitleRightView(imageView,flParams);
    }

    public void setTitleRightView(View curView,FrameLayout.LayoutParams flParams){
        if(getTitleRightVisibility() != View.VISIBLE)setTitleRightVisibility(View.VISIBLE);
        if(null != rightView){
            titleRightFy.removeView(rightView);
            rightView = null;
        }//end of if
        if(null == rightView){
            rightView = curView;
            rightView.setId(StephenToolUtils.generateViewId());
            titleRightFy.addView(rightView,flParams);
        }//end of if
    }

    public View getTitleRightView() {
        return rightView;
    }

    public void setTitleRightClickListener(View.OnClickListener onClickListener){
        titleRightFy.setOnClickListener(onClickListener);
    }

    ////////////////////////////////////////////////////////////////////
    public void setVisibility(int visibility){
        getTopTitleView().setVisibility(visibility);
    }

    public void setTitleClickListener(View.OnClickListener onClickListener){
        titleBgFy.setOnClickListener(onClickListener);
    }

    ////////////////////////////////////////////////////////////////////
    public FrameLayout.LayoutParams getTitleLeftLp() {
        return getTitleLeftLp(20, 20, 12);
    }

    public FrameLayout.LayoutParams getTitleLeftLp(int widthForDp, int heightForDp, int leftMarginDp) {
        FrameLayout.LayoutParams leftLp = new FrameLayout.LayoutParams(isSystemLayoutParams(widthForDp) ? widthForDp : StephenToolUtils.dip2px(activity, widthForDp),
                isSystemLayoutParams(heightForDp) ? heightForDp : StephenToolUtils.dip2px(activity, heightForDp));
        if (leftMarginDp > -1) leftLp.leftMargin = StephenToolUtils.dip2px(activity, leftMarginDp);
        leftLp.gravity = Gravity.CENTER_VERTICAL;
        return leftLp;
    }

    public FrameLayout.LayoutParams getTitleRightLp() {
        return getTitleRightLp(20, 20, 12);
    }

    public FrameLayout.LayoutParams getTitleRightLp(int widthForDp, int heightForDp, int rightMarginDp) {
        FrameLayout.LayoutParams rightLp = new FrameLayout.LayoutParams(isSystemLayoutParams(widthForDp) ? widthForDp : StephenToolUtils.dip2px(activity, widthForDp),
                isSystemLayoutParams(heightForDp) ? heightForDp : StephenToolUtils.dip2px(activity, heightForDp));
        if (rightMarginDp > -1)
            rightLp.rightMargin = StephenToolUtils.dip2px(activity, rightMarginDp);
        rightLp.gravity = Gravity.CENTER_VERTICAL;
        return rightLp;
    }

    public FrameLayout.LayoutParams getTitleCenterLp() {
        return getTitleCenterLp(-1, -1, StephenCommonTopTitleView.TitleHeightForDp / 6);
    }

    public FrameLayout.LayoutParams getTitleCenterLp(int widthForDp, int heightForDp, int topMarginDp) {
        FrameLayout.LayoutParams centerLp = new FrameLayout.LayoutParams((-1 == widthForDp) ?
                ViewGroup.LayoutParams.WRAP_CONTENT :
                (isSystemLayoutParams(widthForDp) ? widthForDp : StephenToolUtils.dip2px(activity, widthForDp)),
                (-1 == heightForDp) ? ViewGroup.LayoutParams.MATCH_PARENT :
                        (isSystemLayoutParams(heightForDp) ? heightForDp : StephenToolUtils.dip2px(activity, heightForDp)));
        centerLp.gravity = Gravity.CENTER;
        if (topMarginDp > -1) centerLp.topMargin = StephenToolUtils.dip2px(activity, topMarginDp);
        return centerLp;
    }

    private boolean isSystemLayoutParams(int val) {
        return (ViewGroup.LayoutParams.WRAP_CONTENT == val || ViewGroup.LayoutParams.MATCH_PARENT == val || ViewGroup.LayoutParams.FILL_PARENT == val);
    }
}