package com.stephen.mycc.launcher.ripple;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;

/**
 * Created by stephen on 3/7/17.
 */
//适用与ListView/GridView这种有子item的
public class StephenRippleItemTool {
    private AbsListView viewGroup;
    private float centerX, centerY;
    private float mRevealRadius = 1, maxRadius;
    private Paint mPaint;
    private int count = 1,speed = 3,delayMillTime = 20;
    private View itemView;
    private int itemPosition;
    private long itemID;
    private boolean isCanClip = true;
    private Rect clipRect;

    public StephenRippleItemTool(AbsListView viewGroup) {
        this.viewGroup = viewGroup;
        mPaint = new Paint();
        mPaint.setColor(Color.parseColor("#55ffffff"));
        mPaint.setAntiAlias(true);
    }

    public void onTouchEvent(MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_UP){
            centerX = event.getX();
            centerY = event.getY();
            mRevealRadius = 1;
        }//end of if
    }

    public boolean performItemClick(View view, int position, long id) {
        itemView = view;
        itemPosition = position;
        itemID = id;
        View item = viewGroup.getChildAt(position - viewGroup.getFirstVisiblePosition());
        clipRect = new Rect(item.getLeft(), item.getTop(), item.getLeft()+item.getWidth(), item.getTop()+item.getHeight());
        float maxX = Math.max(centerX - item.getLeft(), item.getWidth() - centerX - item.getLeft());
        float maxY = Math.max(centerY -item.getTop(), item.getHeight() - centerY-  item.getTop());
        maxRadius = (float) Math.sqrt(maxX*maxX + maxY*maxY);
        viewGroup.invalidate();
        return false;
    }

    protected void dispatchDraw(Canvas canvas,OnSuperPerformClick onSuperPerformClick) {
        if(itemView != null){
            int previewCount = count - 1;
            if(mRevealRadius - previewCount*previewCount > maxRadius){
                if(count !=1){
                    count = 1;
                    viewGroup.invalidate();
                    if(null != onSuperPerformClick)onSuperPerformClick.superPerformClick(itemView, itemPosition, itemID);
                    itemView = null;
                    itemID  = 0;
                }//end of if
                return;
            }//end of if
            canvas.save();
            if(isCanClip)canvas.clipRect(clipRect);
            canvas.drawCircle(centerX, centerY, mRevealRadius , mPaint);
            canvas.restore();
            mRevealRadius += speed * (count * count);
            count++;
            if(delayMillTime > 0){
                viewGroup.postInvalidateDelayed(delayMillTime);
            }else{
                viewGroup.postInvalidate();
            }
        }
    }

    public void setRippleMillTime(int delayMillTime) {
        this.delayMillTime = delayMillTime;
    }

    public void setRippleColor(int color){
        mPaint.setColor(color);
    }

    public void setRippleSpeed(int speed){
        if(speed < 0)speed = 1;
        this.speed = speed;
    }

    public void setCanClip(boolean canClip) {
        isCanClip = canClip;
    }

    public interface OnSuperPerformClick{
        void superPerformClick(View view, int position, long id);
    }
}
