package com.stephen.mycc.launcher.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

public abstract class StephenCommonAdapter<T> extends BaseAdapter {//集成了BaseAdapter所有方法
	protected LayoutInflater mInflater;
	protected Context mContext;
	protected List<T> mDatas;
	protected final int mItemLayoutId;

	public StephenCommonAdapter(Context context, List<T> mDatas, int itemLayoutId) {
		this.mContext = context;
		this.mInflater = LayoutInflater.from(mContext);
		this.mDatas = mDatas;
		this.mItemLayoutId = itemLayoutId;
	}

	@Override
	public int getCount() {
		return mDatas.size();
	}

	@Override
	public T getItem(int position) {
		return mDatas.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final StephenViewHolder stephenViewHolder = getViewHolder(position, convertView,parent);
		convert(stephenViewHolder, getItem(position));
		return stephenViewHolder.getConvertView();

	}

	public abstract void convert(StephenViewHolder helper, T item);

	private StephenViewHolder getViewHolder(int position, View convertView, ViewGroup parent) {
		return StephenViewHolder.get(mContext, convertView, parent, mItemLayoutId,position);
	}


}
