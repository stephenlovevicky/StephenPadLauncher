package com.stephen.mycc.launcher.ripple;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by stephen on 3/7/17.
 */
//适用与Button这种直接的,切忌不能设置给控件复杂的背景,不然影响绘制速度
public class StephenRippleTool {
    private View viewGroup;
    private float centerX ,centerY;
    private float mRevealRadius = 1 ,maxRadius;
    private Paint mPaint;
    private boolean isCanClip = true,isCenterCircle = true;
    private int rippleColor = Color.parseColor("#55ffffff"),count = 1,speed = 1,delayMillTime = 20;
    private RippleClickCallback rippleClickCallback;

    public StephenRippleTool(View viewGroup) {
        this.viewGroup = viewGroup;
        mPaint = new Paint();
        mPaint.setColor(rippleColor);
        mPaint.setAntiAlias(true);

        rippleClickCallback = new RippleClickCallback() {
            private int performClicking;

            @Override
            public boolean performClick(int id) {
                if(performClicking == 0){
                    performClicking = id;
                    return true;
                }//end of if
                return false;
            }

            @Override
            public int getState() {
                return performClicking;
            }

            @Override
            public void doneClick() {
                performClicking = 0;
            }
        };
    }

    public boolean onTouchEvent(MotionEvent event) {
        if(null != rippleClickCallback){
            if(rippleClickCallback.getState() == 0){
                rippleClickCallback.performClick(viewGroup.getId());
            }else if(rippleClickCallback.getState() != viewGroup.getId()){
                return false;
            }//end of else if
        }//end of if
        if(event.getAction() == MotionEvent.ACTION_UP && count == 1){
            if(isCenterCircle){
                centerX = viewGroup.getWidth()/2;centerY = viewGroup.getHeight()/2;
            }else{
                centerX = event.getX();centerY = event.getY();
            }
            mRevealRadius = 1;
            float maxX = Math.max(centerX, viewGroup.getWidth() - centerX	);
            float maxY = Math.max(centerY, viewGroup.getHeight() - centerY);
            maxRadius = (float) Math.sqrt((maxX * maxX) + (maxY*maxY));
            rippleClickCallback.doneClick();
        }else if(event.getAction() == MotionEvent.ACTION_CANCEL){
            rippleClickCallback.doneClick();
        }//end of else if
        return true;
    }

    protected boolean dispatchDraw(Canvas canvas,OnSuperPerformClick onSuperPerformClick) {
        int previewCount = count - 1,curRadius = (int)(mRevealRadius - (previewCount * previewCount));
        //System.out.println(mRevealRadius+"========"+curRadius+"=====>"+maxRadius);
        if(curRadius > maxRadius){
            //System.out.println(mRevealRadius+"======"+maxRadius+"===="+previewCount+"===>"+count);
            if(count != 1){
                count = 1;
                mPaint.setColor(rippleColor);
                viewGroup.invalidate();
                if(null != onSuperPerformClick)onSuperPerformClick.superPerformClick();
                if(null != rippleClickCallback)rippleClickCallback.doneClick();
            }//end of if
            return true;
        }else{
            canvas.save();
            if(isCanClip)canvas.clipRect(0, 0, viewGroup.getWidth(), viewGroup.getHeight());
            if((maxRadius - curRadius) <= (maxRadius/2.8f))mPaint.setColor(Color.TRANSPARENT);
            canvas.drawCircle(centerX, centerY, mRevealRadius, mPaint);
            canvas.restore();
            mRevealRadius += speed * (count * count);
            count++;
            if(delayMillTime > 0){
                viewGroup.postInvalidateDelayed(delayMillTime);
            }else{
                viewGroup.postInvalidate();
            }
        }
        return false;
    }

    public void setRippleMillTime(int delayMillTime) {
        this.delayMillTime = delayMillTime;
    }

    public void setRippleColor(int color){
        rippleColor = color;
        mPaint.setColor(rippleColor);
    }

    public void setRippleSpeed(int speed){
        if(speed < 0)speed = 1;
        this.speed = speed;
    }

    public void setCanClip(boolean canClip) {
        isCanClip = canClip;
    }

    public void setIsCenterCircle(boolean centerCircle) {
        isCenterCircle = centerCircle;
    }

    public interface OnSuperPerformClick{
        void superPerformClick();
    }

    //预防多个波纹效果的按钮同时点击自动触发多个点击事件的问题
    public interface RippleClickCallback {
        boolean performClick(int id) ;
        void doneClick() ;
        int getState();
    }
}
