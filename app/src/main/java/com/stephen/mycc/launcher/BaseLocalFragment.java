package com.stephen.mycc.launcher;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.View;

import com.stephen.mycc.launcher.base.BaseFragment;
import com.stephen.mycc.launcher.base.StephenCommonNoDataView;
import com.stephen.mycc.launcher.base.StephenCommonTopTitleView;
import com.stephen.mycc.launcher.base.StephenToolUtils;

public abstract class BaseLocalFragment extends BaseFragment {
    protected BaseLocalActivity localActivity;//onCreateView里面不能用这个,当时还没初始化
    protected StephenCommonNoDataView stephenCommonNoDataView;
    protected StephenCommonTopTitleView stephenCommonTopTitleView;
    protected BroadcastReceiver timeUpdateBroadcastReceiver;

    @Override
    public void initializeFragmentData() {
        super.initializeFragmentData();
        localActivity = (BaseLocalActivity)activity;
    }

    protected void setMainViewBackground(View mainView, int viewId, int bgResId){
        try{
            StephenToolUtils.setBackgroundAllVersion(findUiViewToInstantiation(mainView,viewId),StephenToolUtils.getBitmapFromResId(activity,bgResId));}catch(Exception e){}
    }

    //绑定每份时间更新广播
    protected void registerSystemTimeMinuteUpdate(){
        if(null == activity)return;
        timeUpdateBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                onSystemTimeMinuteUpdate();
            }
        };

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_TIME_TICK);
        activity.registerReceiver(timeUpdateBroadcastReceiver,intentFilter);
    }

    protected void onSystemTimeMinuteUpdate(){}

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(null != activity && null != timeUpdateBroadcastReceiver)activity.unregisterReceiver(timeUpdateBroadcastReceiver);
    }
}
