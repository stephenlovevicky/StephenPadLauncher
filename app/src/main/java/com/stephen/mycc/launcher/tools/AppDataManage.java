
package com.stephen.mycc.launcher.tools;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import com.stephen.mycc.launcher.bean.AppModel;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AppDataManage {

    private final Context context;

    public AppDataManage(Context context) {
        this.context = context;
    }

    public ArrayList<AppModel> getLaunchAppList() {
        PackageManager localPackageManager = context.getPackageManager();
        Intent localIntent = new Intent("android.intent.action.MAIN");
        localIntent.addCategory("android.intent.category.LAUNCHER");
        List<ResolveInfo> localList = localPackageManager.queryIntentActivities(localIntent, 0);
        ArrayList<AppModel> localArrayList = new ArrayList<>();
        Iterator<ResolveInfo> localIterator = null;
        if(localList.size() > 0)localIterator = localList.iterator();
        if(null != localIterator){
            while(localIterator.hasNext()){
                ResolveInfo localResolveInfo = localIterator.next();
                if(null != localResolveInfo){
                    String packageName = localResolveInfo.activityInfo.packageName;
                    if(!localResolveInfo.equals(context.getPackageName())) {
                        AppModel localAppBean = new AppModel();
                        localAppBean.setPackageName(packageName);
                        localAppBean.setIcon(localResolveInfo.activityInfo.loadIcon(localPackageManager));
                        localAppBean.setName(localResolveInfo.activityInfo.loadLabel(localPackageManager).toString());
                        localAppBean.setDataDir(localResolveInfo.activityInfo.applicationInfo.publicSourceDir);
                        localAppBean.setLauncherName(localResolveInfo.activityInfo.name);
                        try{
                            PackageInfo mPackageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
                            localAppBean.setSysApp((mPackageInfo.applicationInfo.flags & mPackageInfo.applicationInfo.FLAG_SYSTEM) != 0);// 系统预装
                        }catch(Exception e){}
                        localArrayList.add(localAppBean);
                    }//end of if
                }//end of if
            }//end of while
        }//end of if
        return localArrayList;
    }

    public AppModel findActivitiesForPackage(String packageName) {
        PackageManager packageManager = context.getPackageManager();
        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        mainIntent.setPackage(packageName);
        List<ResolveInfo> apps = packageManager.queryIntentActivities(mainIntent, 0);
        if(null != apps && apps.size() > 0){
            ResolveInfo localResolveInfo = apps.get(0);
            if(null != localResolveInfo){
                AppModel localAppBean = new AppModel();
                localAppBean.setIcon(localResolveInfo.activityInfo.loadIcon(context.getPackageManager()));
                localAppBean.setName(localResolveInfo.activityInfo.loadLabel(context.getPackageManager()).toString());
                localAppBean.setPackageName(localResolveInfo.activityInfo.packageName);
                localAppBean.setDataDir(localResolveInfo.activityInfo.applicationInfo.publicSourceDir);
                localAppBean.setLauncherName(localResolveInfo.activityInfo.name);
                try{
                    PackageInfo mPackageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
                    localAppBean.setSysApp((mPackageInfo.applicationInfo.flags & mPackageInfo.applicationInfo.FLAG_SYSTEM) != 0);// 系统预装
                }catch(Exception e){}
                return localAppBean;
            }//end of if
        }//end of if
        return null;
    }
}
