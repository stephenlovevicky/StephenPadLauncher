package com.stephen.mycc.launcher.base;

//全部加载完成事件
public interface FragmentTabAllLoadCompletedListener {
    void onFragmentTabAllLoadCompleted(int lastIndex);
}
