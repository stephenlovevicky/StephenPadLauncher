package com.stephen.mycc.launcher.fragment;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.stephen.mycc.launcher.BaseLocalFragment;
import com.stephen.mycc.launcher.R;
import com.stephen.mycc.launcher.adapter.BottomAppAdapter;
import com.stephen.mycc.launcher.base.StephenToolUtils;
import com.stephen.mycc.launcher.bean.AppModel;
import com.stephen.mycc.launcher.tools.AppDataManage;

import java.util.List;

public class FragmentMainBottom extends BaseLocalFragment {
    private RecyclerView recyclerView;
    private ImageView backLeftImgV;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_bottom, container, false);
        recyclerView = findUiViewToInstantiation(view,R.id.ry_app_list);
        backLeftImgV = findUiViewToInstantiation(view,R.id.backLeftImgV);
        setUiViewClickListener(view,backLeftImgV);

        setMainViewBackground(view,R.id.mainRy,R.drawable.bg_main_bottom);
        return view;
    }

    @Override
    public void initializeFragmentData() {
        backLeftImgV.setVisibility(View.GONE);
        GridLayoutManager gridlayoutManager = new GridLayoutManager(activity, 3);
        gridlayoutManager.setOrientation(GridLayoutManager.HORIZONTAL);
        gridlayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                backLeftImgV.setVisibility(position < 30 ? View.GONE : View.VISIBLE);
                return 1;
            }
        });
        recyclerView.setLayoutManager(gridlayoutManager);
        recyclerView.setFocusable(false);
    }

    @Override
    public boolean getFragmentContentData(Object... objects) {
        List<AppModel> allAppList = (new AppDataManage(activity)).getLaunchAppList();
        if(allAppList.size() <= 0)StephenToolUtils.showShortHintInfo(activity,"没有获取到应用");
        BottomAppAdapter adapter = new BottomAppAdapter(activity, allAppList);
        recyclerView.setAdapter(adapter);
        recyclerView.scrollToPosition(0);
        return super.getFragmentContentData(objects);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()){
            case R.id.backLeftImgV:
                recyclerView.smoothScrollToPosition(0);
                break;
        }//end of switch
    }
}