package com.stephen.mycc.launcher.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.stephen.mycc.launcher.BaseLocalFragment;
import com.stephen.mycc.launcher.R;
import com.stephen.mycc.launcher.adapter.StephenCommonAdapter;
import com.stephen.mycc.launcher.adapter.StephenViewHolder;
import com.stephen.mycc.launcher.bean.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class FragmentMainTop extends BaseLocalFragment {
    private ListView menuList;
    private StephenCommonAdapter<MenuItem> menuAdapter;
    private List<MenuItem> menuItems = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_top, container, false);
        menuList = findUiViewToInstantiation(view,R.id.menu_list);

        setMainViewBackground(view,R.id.mainRy,R.drawable.bg_main_top);
        return view;
    }

    @Override
    public void initializeFragmentData() {
        menuItems.add(new MenuItem("推荐使用",false));
        menuItems.add(new MenuItem("全部应用",false));
        menuItems.add(new MenuItem("推荐使用",false));
        menuItems.add(new MenuItem("全部应用",false));
        menuItems.add(new MenuItem("推荐使用",false));
        menuItems.add(new MenuItem("全部应用",false));
        menuItems.add(new MenuItem("推荐使用",false));
        menuItems.add(new MenuItem("全部应用",false));
        menuItems.add(new MenuItem("推荐使用",false));
        menuItems.add(new MenuItem("全部应用",false));
        menuItems.add(new MenuItem("推荐使用",false));
        menuItems.add(new MenuItem("全部应用",false));
        menuAdapter = new StephenCommonAdapter<MenuItem>(activity, menuItems, R.layout.item_menu_list) {
            @Override
            public void convert(StephenViewHolder helper, MenuItem item) {
                TextView menuT = helper.getView(R.id.menuT);
                menuT.setText(item.getMenuStr());
                menuT.setTextColor(activity.getResources().getColor(item.isSelect() ? R.color.crimson : R.color.salmon));
                //StephenToolUtils.setBackgroundAllVersion(helper.getView(R.id.itemRy),activity.getResources().getDrawable(item.isSelect() ? R.color.transparent_half : R.color.transparent));
            }
        };
        menuList.setAdapter(menuAdapter);
        menuList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position < menuItems.size()){
                    if(menuItems.get(position).isSelect())return;
                    for(int i=0;i<menuItems.size();i++)menuItems.get(i).setSelect(i == position);
                    menuAdapter.notifyDataSetChanged();
                }//end of if
                Fragment fragment = null;
                switch(position){
                    case 0:
                        fragment = new FragmentMainBottom();
                        break;
                    case 1:
                        fragment = new FragmentMainTop();
                        break;
                }//end of switch
                //if(null != fragment)getFragmentManager().beginTransaction().replace(R.id.details_layout, fragment).commit();
            }
        });
    }
}