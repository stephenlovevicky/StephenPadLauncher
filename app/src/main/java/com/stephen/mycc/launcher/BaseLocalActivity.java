package com.stephen.mycc.launcher;

import android.content.Intent;
import android.view.View;

import com.stephen.mycc.launcher.adapter.FragmentTabContentAdapter;
import com.stephen.mycc.launcher.base.BaseActivity;
import com.stephen.mycc.launcher.base.BaseFragment;
import com.stephen.mycc.launcher.base.FragmentTabAllLoadCompletedListener;
import com.stephen.mycc.launcher.base.FragmentTabContentLoadedListener;
import com.stephen.mycc.launcher.base.StephenCommonNoDataView;
import com.stephen.mycc.launcher.base.StephenCommonTopTitleView;

public abstract class BaseLocalActivity extends BaseActivity implements FragmentTabContentLoadedListener,FragmentTabAllLoadCompletedListener {
    protected StephenCommonTopTitleView stephenCommonTopTitleView;
    protected FragmentTabContentAdapter fragmentTabContentAdapter;
    protected StephenCommonNoDataView stephenCommonNoDataView;

    @Override
    public void initializeActivityData() {
        super.initializeActivityData();
    }

    @Override
    public boolean needExitActivity() {//这里过滤哪些界面返回就提示退出
        return (null != activity && (activity instanceof MainActivity));
    }

    @Override
    public void onFragmentTabContentLoaded(int parentIndex, int childIndex) {
        if(-1 != parentIndex && null != fragmentTabContentAdapter)fragmentTabContentAdapter.setCurLoadedCount(parentIndex);
    }

    @Override
    public void onFragmentTabAllLoadCompleted(int lastIndex) {}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(null != fragmentTabContentAdapter){
            for(int i=0;i<fragmentTabContentAdapter.getCount();i++){
                BaseFragment baseFragment = fragmentTabContentAdapter.getFragment(i);
                if(null != baseFragment)baseFragment.execOnActivityResult(requestCode, resultCode, data);//基activityResult
            }//end of for
        }//end of if
    }

    protected void setCommLeftBackBtnClickResponse() {
        if (null != stephenCommonTopTitleView)
            stephenCommonTopTitleView.setTitleLeftClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    backBtnClickResponse();
                }
            });
    }
}
