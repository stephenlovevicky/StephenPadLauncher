package com.stephen.mycc.launcher.base;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stephen on 2016/3/3.
 * 动态注入不影响原布局的无内容提示界面
 */
public class StephenCommonNoDataView {
    private Activity activity;
    private View mainContentView,finalCreateView;
    private FrameLayout mainContainerFy;
    private RelativeLayout mainNoDataRy;
    private TextView centerTV;
    private int imgWidthDp = 81,imgHeightDp = 48,imgPaddingDp = 10;
    private int containerBgColorVal = Color.parseColor("#100f0f");
    private int mainNoDataBgColorVal = Color.TRANSPARENT;
    private int textColorVal = Color.WHITE;
    private String hintStr = "此处应该有数据,点击重试...";
    private boolean isResponseClick = false;
    private List<Integer> bottomBtnIds;

    public StephenCommonNoDataView(Activity activity) {
        this(activity,false);
    }

    public StephenCommonNoDataView(Activity activity, boolean isResponseClick) {
        this.activity = activity;
        mainContainerFy = new FrameLayout(activity);//主容器
        mainContainerFy.setBackgroundColor(containerBgColorVal);
        //create no data view
        mainNoDataRy = new RelativeLayout(activity);
        mainNoDataRy.setBackgroundColor(mainNoDataBgColorVal);
        centerTV = new TextView(activity);
        this.isResponseClick = isResponseClick;
        if(!isResponseClick()){
            hintStr = "此处应该有数据...";
        }else{
            hintStr = "此处应该有数据,点击重试...";
        }
    }

    public View initAndInjectNoDataViewForAllViewReturnView(int mainLayoutId,int replaceViewId) {//注入全部,返回view
        View mainView = LayoutInflater.from(activity).inflate(mainLayoutId,null);
        if(null != mainView)return initAndInjectNoDataViewForAllView(mainView.findViewById(replaceViewId));
        return null;
    }

    public void initAndInjectNoDataViewForAllViewWithActivity(int mainLayoutId,int replaceViewId){//注入全部,直接用于activity
        View mainView = LayoutInflater.from(activity).inflate(mainLayoutId,null);
        if(null == mainView){
            activity.setContentView(mainLayoutId);
        }else{
            activity.setContentView(initAndInjectNoDataViewForAllView(mainView.findViewById(replaceViewId)));
        }
    }

    public View initAndInjectNoDataViewForPartViewReturnView(int mainLayoutId,int replaceViewId,ViewGroup.LayoutParams layoutParams) {//注入部分,返回view
        View mainView = LayoutInflater.from(activity).inflate(mainLayoutId,null);
        if(null != mainView)return initAndInjectNoDataViewForPartView(mainView.findViewById(replaceViewId),mainView,layoutParams);
        return null;
    }

    public void initAndInjectNoDataViewForPartViewWithActivity(int mainLayoutId,int replaceViewId,ViewGroup.LayoutParams layoutParams){//注入部分,直接用于activity
        View mainView = LayoutInflater.from(activity).inflate(mainLayoutId,null);
        if(null == mainView){
            activity.setContentView(mainLayoutId);
        }else{
            activity.setContentView(initAndInjectNoDataViewForPartView(mainView.findViewById(replaceViewId),mainView,layoutParams));
        }
    }

    public View initAndInjectNoDataViewForAllView(int mainLayoutId){
        View mainView = LayoutInflater.from(activity).inflate(mainLayoutId,null);
        if(null != mainView)return initAndInjectNoDataViewForAllView(mainView);
        return null;
    }

    public View initAndInjectNoDataViewForAllView(View mainContentView){//注入全部,可直接用于fragment
        finalCreateView = initInjectSpecificView(mainContentView,null);
        return finalCreateView;
    }

    public View initAndInjectNoDataViewForPartView(View replaceView, View mainView,ViewGroup.LayoutParams layoutParams){//注入部分,可直接用于fragment
        ViewGroup rootView = StephenToolUtils.convertRootViewToViewGroup(mainView);
        int customerLvIndex = StephenToolUtils.getChildViewInRootViewIndex(rootView,mainView,replaceView.getId());
        if(-1 != customerLvIndex)rootView.addView(initInjectSpecificView(replaceView,rootView),customerLvIndex,layoutParams);
        finalCreateView = mainView;
        return finalCreateView;
    }

    private View initInjectSpecificView(View mainContentView,ViewGroup rootView){
        this.mainContentView = mainContentView;
        if(null != rootView)rootView.removeView(mainContentView);//rootView主要用于只替换布局中部分的显示内容,如果有必须在下步之前移除
        mainContainerFy.addView(mainContentView,new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        centerTV.setId(StephenToolUtils.generateViewId());
        centerTV.setText(hintStr);
        centerTV.setGravity(Gravity.CENTER);
        centerTV.setTextSize(15);//sp
        centerTV.setTextColor(textColorVal);
        centerTV.setSingleLine(false);
        RelativeLayout.LayoutParams centerTvLp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        centerTvLp.addRule(RelativeLayout.CENTER_IN_PARENT);
        mainNoDataRy.addView(centerTV,centerTvLp);
        mainContainerFy.addView(mainNoDataRy,new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        setNoDataViewHide();//默认隐藏
        return mainContainerFy;
    }

    //更改提示文本
    public void setCenterTextViewStr(String hintStr){
        this.hintStr = hintStr;
        if(null != centerTV)centerTV.setText(hintStr);
    }

    //提示文本上面的提示图
    public void setCenterTextTopHintImg(int imgResId){
        setCenterTextTopHintImg(imgResId,imgWidthDp,imgHeightDp,imgPaddingDp);
    }

    public void setCenterTextTopHintImg(int imgResId,int imgWidthDp,int imgHeightDp,int imgPaddingDp){
        StephenToolUtils.setTextViewAroundDrawable(activity,centerTV,imgResId,imgWidthDp,imgHeightDp,imgPaddingDp, Gravity.TOP);
    }

    public void setCenterTextTopHintImg(Drawable imgDrawable){
        setCenterTextTopHintImg(imgDrawable,imgWidthDp,imgHeightDp,imgPaddingDp);
    }

    public void setCenterTextTopHintImg(Drawable imgDrawable, int imgWidthDp, int imgHeightDp, int imgPaddingDp){
        StephenToolUtils.setTextViewAroundDrawable(activity,centerTV,imgDrawable,imgWidthDp,imgHeightDp,imgPaddingDp,Gravity.TOP);
    }

    //提示文本和提示图一起设置
    public void setCenterTextStrAndHintImg(String hintStr,int imgResId,int imgWidthDp,int imgHeightDp,int imgPaddingDp){
        setCenterTextViewStr(hintStr);
        setCenterTextTopHintImg(imgResId,imgWidthDp,imgHeightDp,imgPaddingDp);
    }

    //提示文本下面的按钮,支持多次设置
    public void setCenterTextBottomBtn(String btnText,int btnColor,Drawable btnBgSelector,int imgWidthDp,int imgHeightDp,int imgMarginDp,View.OnClickListener onClickListener){
        if(null != mainNoDataRy){
            Button bottomBtn = new Button(activity);
            bottomBtn.setId(StephenToolUtils.generateViewId());
            bottomBtn.setGravity(Gravity.CENTER);
            bottomBtn.setText(btnText);
            bottomBtn.setTextColor(btnColor);
            bottomBtn.setSingleLine(true);
            StephenToolUtils.setBackgroundAllVersion(bottomBtn,btnBgSelector);
            RelativeLayout.LayoutParams bottomBtnLp = new RelativeLayout.LayoutParams(StephenToolUtils.dip2px(activity,imgWidthDp), StephenToolUtils.dip2px(activity,imgHeightDp));
            bottomBtnLp.addRule(RelativeLayout.CENTER_HORIZONTAL);
            bottomBtnLp.setMargins(0,StephenToolUtils.dip2px(activity,imgMarginDp),0,0);
            if(null == bottomBtnIds){
                if(null != centerTV)bottomBtnLp.addRule(RelativeLayout.BELOW,centerTV.getId());
                bottomBtnIds = new ArrayList<Integer>();
            }else{
                bottomBtnLp.addRule(RelativeLayout.BELOW,bottomBtnIds.get(bottomBtnIds.size()-1));
            }
            bottomBtnIds.add(bottomBtn.getId());
            mainNoDataRy.addView(bottomBtn,bottomBtnLp);
            bottomBtn.setOnClickListener(onClickListener);
        }//end of if
    }

    public void setMainContainerBgColorVal(int bgColorVal) {
        this.containerBgColorVal = bgColorVal;
        mainContainerFy.setBackgroundColor(activity.getResources().getColor(containerBgColorVal));
    }

    public void setMainNoDataBgColorVal(int bgColorVal) {
        this.mainNoDataBgColorVal = bgColorVal;
        mainNoDataRy.setBackgroundColor(activity.getResources().getColor(mainNoDataBgColorVal));
    }

    public void setTextColorVal(int textColorVal) {
        this.textColorVal = textColorVal;
    }

    public void setNoDataViewShow(){
        setNoDataViewShow(true,hintStr);
    }

    public void setNoDataViewShow(String hintStr){
        setNoDataViewShow(true, hintStr);
    }

    public void setNoDataViewShow(boolean isResponseClick,String hintStr){
        this.isResponseClick = isResponseClick;
        setCenterTextViewStr(hintStr);
        if(null != mainContentView)if(mainContentView.getVisibility() != View.GONE)mainContentView.setVisibility(View.GONE);
        if(null != mainNoDataRy)if(mainNoDataRy.getVisibility() != View.VISIBLE)mainNoDataRy.setVisibility(View.VISIBLE);
    }

    public void setNoDataViewHide(){
        setResponseClick(true);
        if(null != mainNoDataRy)if(mainNoDataRy.getVisibility() != View.GONE)mainNoDataRy.setVisibility(View.GONE);
        if(null != mainContentView)if(mainContentView.getVisibility() != View.VISIBLE)mainContentView.setVisibility(View.VISIBLE);
    }

    public void setResponseClick(boolean responseClick) {
        isResponseClick = responseClick;
    }

    public boolean isResponseClick() {
        return isResponseClick;
    }

    public FrameLayout getMainContainerView() {
        return mainContainerFy;
    }

    public RelativeLayout getNoDataMainView() {
        return mainNoDataRy;
    }

    public View getFinalCreateView(){
        return finalCreateView;
    }

    public void setOnNoDataViewClickListener(final OnNoDataViewClickListener onClickListener){
        if(null != mainNoDataRy)mainNoDataRy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isResponseClick()){
                    setNoDataViewHide();
                    if(null != onClickListener)onClickListener.onNoDataViewClick();
                }//end of if
            }
        });
    }

    public interface OnNoDataViewClickListener{
        void onNoDataViewClick();
    }
}