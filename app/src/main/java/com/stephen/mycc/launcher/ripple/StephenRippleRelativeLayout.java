package com.stephen.mycc.launcher.ripple;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

public class StephenRippleRelativeLayout extends RelativeLayout{
    private StephenRippleTool stephenRippleTool;

	public StephenRippleRelativeLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
        stephenRippleTool = new StephenRippleTool(this);
	}

	public StephenRippleRelativeLayout(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public StephenRippleRelativeLayout(Context context) {
		this(context , null);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
        if(!stephenRippleTool.onTouchEvent(event))return false;
		return super.onTouchEvent(event);
	}

	@Override
	public boolean performClick(){
		invalidate();
		return false;
	}

	@Override
	protected void dispatchDraw(Canvas canvas) {
		super.dispatchDraw(canvas);
		stephenRippleTool.dispatchDraw(canvas, new StephenRippleTool.OnSuperPerformClick() {
            @Override
            public void superPerformClick() {
                callSuperPerformClick();
            }
        });
	}

    private void callSuperPerformClick(){
        super.performClick();
    }

	public void setRippleMillTime(int delayMillTime) {
		stephenRippleTool.setRippleMillTime(delayMillTime);
	}

	public void setRippleColor(int color){
		stephenRippleTool.setRippleColor(color);
	}

    public void setRippleSpeed(int speed){
        stephenRippleTool.setRippleSpeed(speed);
    }

    public void setRippleCanClip(boolean canClip) {
        stephenRippleTool.setCanClip(canClip);
    }
}
