package com.stephen.mycc.launcher.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.view.ViewGroup;

import com.stephen.mycc.launcher.MainActivity;
import com.stephen.mycc.launcher.base.BaseFragment;
import com.stephen.mycc.launcher.base.FragmentTabAllLoadCompletedListener;
import com.stephen.mycc.launcher.fragment.FragmentMainTop;
import com.stephen.mycc.launcher.fragment.FragmentMainCenter;
import com.stephen.mycc.launcher.fragment.FragmentMainBottom;

public class FragmentTabContentAdapter extends FragmentStatePagerAdapter {
    public static final int FragmentMainVp = 0;
    public static boolean needReload = false;
    private FragmentManager fm;
    private Bundle paramBundle = null;
    private BaseFragment[] fragmentAry = null;
    private int curShowIndex = 0, fragmentFlag = -1, tabTitleCount = 0;
    private boolean[] tabLoadedAry;
    private FragmentTabAllLoadCompletedListener tabAllLoadCompletedListener;

    public FragmentTabContentAdapter(FragmentManager fm, int fragmentFlag, int tabTitleCount, Bundle paramBundle, FragmentTabAllLoadCompletedListener tabAllLoadCompletedListener) {
        super(fm);
        this.fm = fm;
        this.paramBundle = (null != paramBundle) ? (paramBundle.isEmpty() ? null : paramBundle) : null;
        this.fragmentFlag = fragmentFlag;
        this.tabTitleCount = tabTitleCount;
        this.tabLoadedAry = new boolean[tabTitleCount];
        for (int i = 0; i < tabTitleCount; i++) this.tabLoadedAry[i] = false;
        this.fragmentAry = new BaseFragment[tabTitleCount];
        this.tabAllLoadCompletedListener = tabAllLoadCompletedListener;
    }

    @Override
    public Fragment getItem(int position) {
        BaseFragment fragment = null;
        switch (fragmentFlag) {
            case FragmentMainVp:
                switch (position) {
                    case MainActivity.MainTop:
                        fragment = new FragmentMainTop();
                        break;
                    case MainActivity.MainCenter:
                        fragment = new FragmentMainCenter();
                        break;
                    case MainActivity.MainBottom:
                        fragment = new FragmentMainBottom();
                        break;
                }//end of switch
                break;
        }//end of switch
        Bundle bundle = new Bundle();
        bundle.putInt(BaseFragment.ParamIndex, position);
        if (null != paramBundle) bundle.putBundle(BaseFragment.ParamBundle, paramBundle);
        if (null != fragment) fragment.setArguments(bundle);
        if (null != fragmentAry) fragmentAry[position] = fragment;
        return fragment;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);//得到缓存的fragment
        if (needReload && null != fragment && null != fm) {
            String fragmentTag = fragment.getTag();//得到tag,这点很重要
            FragmentTransaction ft = fm.beginTransaction();
            if (null != ft) {
                ft.remove(fragment);//移除旧的fragment
                fragment = getItem(position);//换成新的fragment
                ft.add(container.getId(), fragment, fragmentTag);//添加新fragment时必须用前面获得的tag,这点很重要
                ft.attach(fragment);
                fragment.setUserVisibleHint(true);
                ft.commit();
            }//end of if
            if((getCount() - 1) == position)needReload = false;//清除更新标记(只有重新启动的时候需要去创建新的fragment对象),防止正常情况下频繁创建对象
        }//end of if
        return fragment;
    }

    @Override
    public int getCount() {
        return tabTitleCount;
    }

    public int getCurShowIndex() {
        return curShowIndex;
    }

    public void setCurShowIndex(int curShowIndex) {
        this.curShowIndex = curShowIndex;
    }

    public void setCurLoadedCount(int index) {
        if (null == tabLoadedAry || index >= tabLoadedAry.length) return;
        tabLoadedAry[index] = true;
        if (null != tabAllLoadCompletedListener) {
            boolean allTabLoadCompleted = true;
            for (int i = 0; i < tabLoadedAry.length; i++) {
                if (!tabLoadedAry[i]) {
                    allTabLoadCompleted = false;
                    break;
                }//end of if
            }//end of for
            if(allTabLoadCompleted)tabAllLoadCompletedListener.onFragmentTabAllLoadCompleted(index);
        }//end of if
    }

    public BaseFragment getCurFragment() {
        return getFragment(curShowIndex);
    }

    public BaseFragment getFragment(int index) {
        if (null != fragmentAry && index < fragmentAry.length) return fragmentAry[index];
        return null;
    }
}

