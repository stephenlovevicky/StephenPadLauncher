package com.stephen.mycc.launcher.ripple;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.GridView;

public class StephenRippleGridView extends GridView{
    private boolean isOnceMaskClick = false;
    private StephenRippleItemTool stephenRippleItemTool;

	public StephenRippleGridView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
        stephenRippleItemTool = new StephenRippleItemTool(this);
		setSelector(new ColorDrawable(Color.TRANSPARENT)) ;
	}

    public StephenRippleGridView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public StephenRippleGridView(Context context) {
        this(context , null);
    }

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		stephenRippleItemTool.onTouchEvent(event);
		return super.onTouchEvent(event);
	}

	@Override
	public boolean performItemClick(View view, int position, long id) {
		return stephenRippleItemTool.performItemClick(view, position, id);
	}

	@Override
	protected void dispatchDraw(Canvas canvas) {
		super.dispatchDraw(canvas);
        stephenRippleItemTool.dispatchDraw(canvas, new StephenRippleItemTool.OnSuperPerformClick() {
            @Override
            public void superPerformClick(View view, int position, long id) {
                if(!isOnceMaskClick){
                    callSuperPerformClick(view, position, id);
                }else{
                    isOnceMaskClick = false;
                }
            }
        });
	}

    private void callSuperPerformClick(View view, int position, long id){
        super.performItemClick(view, position, id);
    }

    public void setOnceMaskClick(boolean onceMaskClick) {
        isOnceMaskClick = onceMaskClick;
    }

    public void setRippleMillTime(int delayMillTime) {
        stephenRippleItemTool.setRippleMillTime(delayMillTime);
    }

    public void setRippleColor(int color){
        stephenRippleItemTool.setRippleColor(color);
    }

    public void setRippleSpeed(int speed){
        stephenRippleItemTool.setRippleSpeed(speed);
    }

    public void setRippleCanClip(boolean canClip) {
        stephenRippleItemTool.setCanClip(canClip);
    }
}
