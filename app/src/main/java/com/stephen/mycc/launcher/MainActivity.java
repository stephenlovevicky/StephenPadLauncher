package com.stephen.mycc.launcher;

import android.os.Bundle;

import com.stephen.mycc.launcher.adapter.FragmentTabContentAdapter;
import com.tmall.ultraviewpager.UltraViewPager;

public class MainActivity extends BaseLocalActivity{
    public static final int MainTop = 0,MainCenter = 1,MainBottom = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainInitFunction(this);
    }

    @Override
    protected void initializeFunction() {
        setContentView(R.layout.activity_main);

        FragmentTabContentAdapter fragmentTabContentAdapter = new FragmentTabContentAdapter(getSupportFragmentManager(), FragmentTabContentAdapter.FragmentMainVp, 3, null, this);

        UltraViewPager ultraViewPager = (UltraViewPager)findViewById(R.id.ultra_viewpager);
        ultraViewPager.setScrollMode(UltraViewPager.ScrollMode.VERTICAL);
        ultraViewPager.setAdapter(fragmentTabContentAdapter);

        ultraViewPager.setCurrentItem(MainCenter);
    }
}