package com.stephen.mycc.launcher.base;

//每页加载完成事件
public interface FragmentTabContentLoadedListener {
    void onFragmentTabContentLoaded(int parentIndex, int childIndex);
}
