package com.stephen.mycc.launcher.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.stephen.mycc.launcher.BaseLocalFragment;
import com.stephen.mycc.launcher.R;
import com.stephen.mycc.launcher.base.StephenCommonTopTitleView;
import com.stephen.mycc.launcher.base.StephenToolUtils;
import com.stephen.mycc.launcher.tools.DateUtil;

public class FragmentMainCenter extends BaseLocalFragment {
    private TextView timeInfoT,dateInfoT;
    private ImageView app0LogoImgV,app1LogoImgV,app2LogoImgV,app3LogoImgV,app4LogoImgV;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_center, container, false);

        View titleBarView = LayoutInflater.from(activity).inflate(R.layout.title_bar_content,null);

        stephenCommonTopTitleView = new StephenCommonTopTitleView(activity,50);
        stephenCommonTopTitleView.setTitleBgColor(R.color.transparent);
        stephenCommonTopTitleView.setTitleRightView(titleBarView, stephenCommonTopTitleView.getTitleRightLp(ViewGroup.LayoutParams.WRAP_CONTENT, 40, 10));

        timeInfoT = findUiViewToInstantiation(titleBarView,R.id.timeInfoT);
        dateInfoT = findUiViewToInstantiation(titleBarView,R.id.dateInfoT);
        app0LogoImgV = findUiViewToInstantiation(view,R.id.app0LogoImgV);
        app1LogoImgV = findUiViewToInstantiation(view,R.id.app1LogoImgV);
        app2LogoImgV = findUiViewToInstantiation(view,R.id.app2LogoImgV);
        app3LogoImgV = findUiViewToInstantiation(view,R.id.app3LogoImgV);
        app4LogoImgV = findUiViewToInstantiation(view,R.id.app4LogoImgV);
        setUiViewClickListener(timeInfoT,dateInfoT);

        setMainViewBackground(view,R.id.mainRy,R.drawable.bg_main_center);
        return stephenCommonTopTitleView.injectCommTitleViewToAllViewReturnView(view,true,true);
    }

    @Override
    public void initializeFragmentData() {
        timeInfoT.setText(DateUtil.getSystemCurDateTime("yyyy年MM月dd日 HH:mm"));
        dateInfoT.setText(DateUtil.getIsWeekInDayShow("yyyy-MM-dd",DateUtil.getSystemCurDateTime("yyyy-MM-dd"),"星期"));
        app0LogoImgV.setImageBitmap(StephenToolUtils.getBitmapFromResId(activity,R.drawable.img_main_video));
        app1LogoImgV.setImageBitmap(StephenToolUtils.getBitmapFromResId(activity,R.drawable.img_main_music));
        app2LogoImgV.setImageBitmap(StephenToolUtils.getBitmapFromResId(activity,R.drawable.img_main_amap));
        app3LogoImgV.setImageBitmap(StephenToolUtils.getBitmapFromResId(activity,R.drawable.img_main_amap));
        app4LogoImgV.setImageBitmap(StephenToolUtils.getBitmapFromResId(activity,R.drawable.img_main_amap));
    }

    @Override
    public boolean getFragmentContentData(Object... objects) {
        registerSystemTimeMinuteUpdate();
        return super.getFragmentContentData(objects);
    }

    @Override
    protected void onSystemTimeMinuteUpdate() {
        super.onSystemTimeMinuteUpdate();
        try {
            timeInfoT.setText(DateUtil.getSystemCurDateTime("yyyy年MM月dd日 HH:mm"));
            dateInfoT.setText(DateUtil.getIsWeekInDayShow("yyyy-MM-dd",DateUtil.getSystemCurDateTime("yyyy-MM-dd"),"星期"));
        }catch(Exception ex){}
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        StephenToolUtils.showShortHintInfo(activity,"click");
    }
}