package com.stephen.mycc.launcher.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.stephen.mycc.launcher.R;
import com.stephen.mycc.launcher.base.StephenToolUtils;
import com.stephen.mycc.launcher.bean.AppModel;

import java.util.List;

public class BottomAppAdapter extends RecyclerView.Adapter<BottomAppAdapter.ViewHolder> {
    private Context context;
    private List<AppModel> appModels;

    public BottomAppAdapter(Context context, List<AppModel> appModels) {
        super();
        this.context = context;
        this.appModels = appModels;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_bottom_app, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder,final int i) {
        if(appModels.size() == 0)return;
        final AppModel appModel = appModels.get(i);
        viewHolder.appNameT.setText(appModel.getName());
        viewHolder.appLogoImgV.setImageDrawable(appModel.getIcon());
        viewHolder.appFlagImgV.setImageDrawable(StephenToolUtils.bitmapToDrawable(StephenToolUtils.getBitmapFromResId(context,appModel.isSysApp() ? R.drawable.icon_system_flag2 : R.drawable.icon_system_flag3)));
        viewHolder.appFlagT.setText(appModel.isSysApp() ? "系统" : "用户");
        viewHolder.itemLy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StephenToolUtils.startAplication(context,appModel.getPackageName(),appModel.getName(),false);
            }
        });
    }

    @Override
    public int getItemCount() {
        return appModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout itemLy;
        public ImageView appLogoImgV,appFlagImgV;
        public TextView appNameT,appFlagT;

        public ViewHolder(View itemView) {
            super(itemView);
            itemLy = (LinearLayout) itemView.findViewById(R.id.itemLy);
            appLogoImgV = (ImageView) itemView.findViewById(R.id.appLogoImgV);
            appNameT = (TextView) itemView.findViewById(R.id.appNameT);
            appFlagImgV = (ImageView) itemView.findViewById(R.id.appFlagImgV);
            appFlagT = (TextView) itemView.findViewById(R.id.appFlagT);
        }
    }
}
