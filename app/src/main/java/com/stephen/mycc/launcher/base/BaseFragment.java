package com.stephen.mycc.launcher.base;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.View;

import java.io.Serializable;

/**
 * bean Fragment 继承了Fragment
 */
public class BaseFragment extends Fragment implements View.OnClickListener{
    public static final String ParamIndex = "ParamIndex",ParamBundle = "ParamBundle",ParamBase = "ParamBase";
    protected BaseActivity activity;
    protected boolean isInitData = false,isCallLoadListener = true;
    protected StephenCommonNoDataView stephenCommonNoDataView;
    protected FragmentTabContentLoadedListener fragmentTabContentLoadedListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (BaseActivity)activity;
        try{
            this.fragmentTabContentLoadedListener = (FragmentTabContentLoadedListener)activity;
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        checkInitializationData();
        if(isCallLoadListener && null != fragmentTabContentLoadedListener && null != getArguments())fragmentTabContentLoadedListener
                .onFragmentTabContentLoaded(getArguments().getInt(ParamIndex,-1),-1);
    }

    //得到view实例,用于fragment
    public <T extends View>T findUiViewToInstantiation(View mainView, int viewId){
        return (T)mainView.findViewById(viewId);
    }

    //设置监听事件
    public void setUiViewClickListener(View... views){
        if(null != views && views.length > 0){
            if(views.length > 1){
                for(View view : views)if(null != view)view.setOnClickListener(this);
            }else{
                if(null != views[0])views[0].setOnClickListener(this);
            }
        }//end of if
    }

    //设置监听事件,用于fragment
    public void setUiViewClickListener(View mainView, int... viewIds){
        if(null != mainView && null != viewIds && viewIds.length > 0){
            if(viewIds.length > 1){
                for(int viewId : viewIds)if(null != findUiViewToInstantiation(mainView,viewId))findUiViewToInstantiation(mainView,viewId).setOnClickListener(this);
            }else{
                if(null != findUiViewToInstantiation(mainView,viewIds[0]))findUiViewToInstantiation(mainView,viewIds[0]).setOnClickListener(this);
            }
        }//end of if
    }

    public void checkInitializationData(){//初使化Fragment数据
        if(!isInitData && null != activity){
            initializeFragmentData();
            getFragmentContentData();
            isInitData = true;
        }//end of if
    }

    public void initializeFragmentData(){}//会自动调用

    public boolean getFragmentContentData(Object... objects){//初使化Fragment数据
        return true;
    }//会自动调用

    public void reInitializationRefreshData(Object... objects){//有需要手动调用
        initializeFragmentData();
        getFragmentContentData(objects);
    }

    public void getFragmentFilterContent(Object... objects){}//有需要手动调用

    public void setCallLoadListener(boolean callLoadListener) {
        isCallLoadListener = callLoadListener;
    }

    //适配器里面传来的,判空处理
    public Bundle getParamBundle(){
        if(null != getArguments()){
            Bundle paramBundle = getArguments().getBundle(ParamBundle);
            if(null != paramBundle)return paramBundle;
        }//end of if
        return new Bundle();
    }

    //获取传递来的序列化对象
    public Serializable getParamBundleSerializableObject(){
        return getParamBundle().getSerializable(ParamBase);
    }

    //返回时检查操作
    public Boolean backCheckOperation() {
        return null;//default
    }

    public void execOnActivityResult(int requestCode, int resultCode, Intent data){}

    public void execDisposeMainHandlerCallMethod(Message msg){}

    public void execRequestWebServerSuccess(int requestWebCode, String resultStr){
        //if(null != stephenCommonXRefreshView)stephenCommonXRefreshView.stopRefresh();
        //if(null != stephenCommonXRefreshView)stephenCommonXRefreshView.stopLoadMore();
    }

    public void execRequestWebServerSuccess(int requestWebCode, Object resultObj) {}

    public void execRequestWebServerFailure(int requestWebCode, String strMsg){
        //if(null != stephenCommonXRefreshView)stephenCommonXRefreshView.stopRefresh();
        //if(null != stephenCommonXRefreshView)stephenCommonXRefreshView.stopLoadMore();
    }

    @Override
    public void onClick(View view) {
        if(null != activity)activity.hideSystemInputMethod(view);
    }
}
