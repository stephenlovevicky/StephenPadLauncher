package com.stephen.mycc.launcher.bean;

public class MenuItem {
    private String menuStr;
    private boolean isSelect;

    public MenuItem(String menuStr, boolean isSelect) {
        this.menuStr = menuStr;
        this.isSelect = isSelect;
    }

    public String getMenuStr() {
        return menuStr;
    }

    public void setMenuStr(String menuStr) {
        this.menuStr = menuStr;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }
}
